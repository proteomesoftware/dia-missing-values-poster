# Enhancing Quantitative Data-Independent-Acquisition Proteomics of Partially Heterogeneous Samples with Missing Value Imputation

## Authors

- Seth Just (Proteome Software) (presenting)
- Kevin Calaway (Proteome Software)
- Caleb Emmons (Proteome Software)
- Susan Ludwigsen (Proteome Software)
- Brian Searle (OSU, Proteome Software)

## Introduction

Relative quantification of peptides is a common application of data-independent-acquisition mass spectrometry (DIA). One advantage of DIA over data-dependent acquisition (DDA) is the lower incidence of missing values due to inconsistent measurement or data processing steps, especially when employing strict control of false discovery rates (FDR). However missing values can still be encountered in DIA results due to noise filtering/suppression (so-called “left censorship” of observed intensities), or when analyzing partially-heterogenous mixtures where some analytes can only be measured in a subset of samples. The consistency of DIA acquisition means missing values are usually caused by low intensity or absence of the analyte, while those encountered in DDA data are more likely to be missing-at-random.

## Methods

Previous work on DIA quantification has often assumed samples are homogenous, or that measurements of low-intensity or absent analytes will reflect instrument noise and be suitable for downstream analysis. However such background integration can also include interference which heavily impacts results. We have developed a relative protein quantification workflow for DIA including a configurable threshold for determining the absence of analytes in individual samples, missing-value imputation, and imputation-aware estimation of the statistical significance of quantitative changes. Missing values are imputed by quantile regression for left-censored data (QRILC), which draws values from a truncated log-normal distribution fit to the non-missing measurements for an analyte (in all categories). Significance estimates use degrees of freedom reduced by the number of imputed values.

## Preliminary data

We initially analyzed the relationship between protein-level intensity and coefficient of variation (CV). We acquired two sets of three replicates, one with DDA techniques, and the other in parallel using DIA. Analyzing each set of samples separately, we controlled protein FDR at 1%, and computed the relationship between intensity and CV for proteins with measured intensities in all three replicates by determining the median CV and a 50% confidence interval in a sliding window over intensities. For analytes with a single missing value, we computed the mean and CV using QRILC. This allowed us to compare the agreement between the intensity-CV relationship deduced from all-present data and that deduced from analytes with missing values, and we found good agreement. For DIA, 10 of 22 QRLIC-produced points fall inside the 50% confidence interval estimated from proteins with a quantitative value in each sample. We saw a larger number of analytes with missing values in the DDA experiment, and the distribution of QRILC-computed CVs seemed broader than that determined from proteins without missing values. We expect more missing values, but the increased spread of CVs may indicate more DDA missing values are missing-at-random. We are currently analyzing DIA samples consisting of a mix of proteomes at known ratios (including complete absence). The heterogeneity of these samples at extreme ratios will allow us to assess the sensitivity and specificity of statistical significance estimates when faced with missing values due to instrument and data processing limitations or at different thresholds. We plan to additionally analyze DIA samples where a subset contains a pathogen infection, introducing an additional set of analytes in only that subset. These analyses will assess the performance tradeoff of different threshold and statistical significance techniques in a real-world experiment. We will also compare the performance of QRILC with other imputation techniques.

## Novel Aspect

We demonstrate that missing value imputation can enhance DIA-MS proteomics analysis for homogeneous or partially-heterogeneous samples without introducing statistical biases.
