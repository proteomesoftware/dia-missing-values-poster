#!/bin/bash
cat > $1_converted.tex << EOF
\documentclass{standalone}
\usepackage{graphicx}
\usepackage{color}
\input{../preamble.tex}
\begin{document}
\fontsize{18}{22.5}\selectfont
\input{changeme.pdf_tex}
\end{document}
EOF

sed -i -e "s|changeme|$1|g" $1_converted.tex
pdflatex --interaction=nonstopmode $1_converted.tex
rm $1_converted.aux $1_converted.log $1_converted.tex
exit 0

# see https://tex.stackexchange.com/a/337112/11899
