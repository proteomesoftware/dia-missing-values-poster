# Enhancing Quantitative Data-Independent-Acquisition Proteomics of Partially Heterogeneous Samples with Missing Value Imputation

Poster for presentation at ASMS 2021.

See [our whitepaper](https://proteomesoftware.bitbucket.io/dia-missing-value-poster/) for more information and to download the full poster.

To learn more, see [http://www.proteomesoftware.com/dia](http://www.proteomesoftware.com/dia), or [contact us](mailto:info@proteomesoftware.com).

## Copyright and License

Poster contents (text and images) are licensed [CC Attribution-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/).
Institution logos are copyright their respective owners and may only be used according to each institution's guidelines.

Code and other content in this repository is copyright Proteome Software Inc (2019-2021), all rights reserved unless otherwise noted.

Poster template and modifications in `main.tex` are licensed GPL-3.0, from [https://github.com/rafaelbailo/betterposter-latex-template]().
