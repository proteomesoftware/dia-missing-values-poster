---
title: |
  | Interesting Title
  |
  | ASMS YEAR --- Day123
author: Seth Just$^1$, Kevin Calaway$^1$, Caleb Emmons$^1$, Susan Ludwigsen$^1$ and Brian Searle$^{1,2}$
institute: $^1$Proteome Software, $^2$The Ohio State University
---

# Title Slide

---

## Slide header

Slide contents

. . .

…and more after a pause

## Thank you

![](img/qrcode.png){ width=40% }

See our whitepaper at: [proteomesoftware.bitbucket.io/PROJECT/](https://proteomesoftware.bitbucket.io/PROJECT/)

---

## See our ASMS 2020 poster

[![Our poster](img/main.png "Click to download our poster"){ width=100% }](https://bitbucket.org/proteomesoftware/REPO/downloads/POSTER.pdf)


# Follow-up

---

## Follow-up

