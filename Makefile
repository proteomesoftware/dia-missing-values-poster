MAIN := main

# Specify what we want built by default. We have to specify the main file, even though it's
# a prereq of the compressed file because make automatically deletes intermediate files.

all: $(MAIN).pdf whitepaper.html whitepaper.pdf slides.pdf
# $(MAIN)-compressed.pdf $(MAIN)-compressed-printer.pdf
# whitepaper-compressed.pdf whitepaper-compressed-printer.pdf

.PHONY: all clean reallyclean encyclopedia-entrap

BIB := references.bib

LATEX := pdflatex --interaction=nonstopmode
REDIR := 1>/dev/null 2>/dev/null
BIBTEX := bibtex
RM := rm -f

SHELL := /bin/bash

RERUN := "(There were undefined references|Rerun to get ((cross-references|the bars) right|citations correct))"
RERUNBIB := "No file.*\.bbl|Citation.*undefined"

#.SILENT:

#####################
# BEGIN IMAGE RULES #
#####################

IMGS := img/by-nc-sa.pdf img/qrcode.png img/scaffold_proteome_software_new_logo_2019_solid_white.pdf img/proportion-filtered-ribosome.pdf img/proportion-filtered-tb-8mz.pdf img/proportion-filtered-plasmodium.pdf img/proportion-quantified-aqua-low-area.pdf img/number-significant-aqua.pdf

WPIMGS := img/main.png img/proportion-filtered-tb-8mz.png img/proportion-filtered-plasmodium.png img/proportion-quantified-aqua-low-area.png img/number-significant-aqua.png

SLIDEIMGS := $(WPIMGS) 

#img/training.png: img/training_converted.pdf
#       inkscape --export-area-page --export-dpi=72 --export-png=$@ $<
#
#img/training_converted.pdf: img/training.pdf_tex img/standalone-pdftex.sh
#       cd img; bash standalone-pdftex.sh training

img/main.png: main.pdf
	inkscape --export-area-page -w 1080 --export-png=$@ $<

%.nbconvert.ipynb: %.ipynb
	. ~/python/jupyterlab-venv/bin/activate && time jupyter-nbconvert --to notebook --execute --ExecutePreprocessor.timeout=-1 $< 

%.nbconvert.pdf: %.nbconvert.ipynb
	jupyter-nbconvert --to pdf --output $* $<

# now fallback to generating pdf images from SVGs

img/%.pdf: img/%.svg
	inkscape --export-area-page --export-pdf=$@ $<

img/%.pdf_tex: img/%.svg
	inkscape --export-area-page --export-latex --export-pdf=img/$*.pdf $<

img/%.png: img/%.svg
	inkscape --export-area-page --export-png=$@ $<

####################
# END IMAGE RULES  #
####################

%-compressed.pdf: %.pdf
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/default -sOutputFile=$@ $<

%-compressed-printer.pdf: %.pdf
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -dPDFSETTINGS=/printer -sOutputFile=$@ $<

#####################
# BEGIN LATEX RULES #
#####################

$(MAIN).pdf: preamble.tex center.tex left.tex right.tex $(IMGS)

%.pdf: %.tex %.aux #%.bbl %.blg
	$(LATEX) $<; true # will NOT fail the build on error
	egrep $(RERUN) $*.log && $(LATEX) $< $(REDIR); true
	egrep $(RERUN) $*.log && $(LATEX) $< $(REDIR); true
	echo "*** Errors for $< ***"; true
	egrep -i "((Reference|Citation).*undefined|Unaddressed TODO)" $*.log ; true

%.aux: %.tex
	$(LATEX) $< $(REDIR); true

%.bbl %.blg: %.aux $(BIB)
# Dependence on the aux makes sure we rerun when changed, but will fire too often
	$(BIBTEX) $* $(REDIR); true
	$(LATEX) $* $(REDIR); true
	egrep -c $(RERUNBIB) $*.log $(REDIR) && ($(BIBTEX) $* $(REDIR);$(LATEX) $* $(REDIR)) ; true

####################
# END LATEX RULES  #
####################

#######################
# BEGIN PANDOC RULES  #
#######################

# Need --standalone to write encoding information to HTML
#PANDOC = pandoc --from markdown --toc --standalone --metadata "date=`hg log -fl 1 --template \"{date(date, '%B %e, %Y')}\"`" --citeproc 
PANDOC := pandoc --from markdown --standalone --bibliography $(BIB) --metadata "date=`git log -1 --date=format:"%B %e, %Y" --format="%ad"`" --citeproc

whitepaper.pdf: $(WPIMGS)

whitepaper.html: $(WPIMGS) pandoc.css

slides.pdf: slides.md $(SLIDEIMGS)
	$(PANDOC) --to beamer --default-image-extension=pdf -o $@ $<

# Fallback rule for PDFs that have Markdown sources
%.pdf: %.md $(BIB) pandoc-template.tex
	$(PANDOC) --to latex --default-image-extension=png --template=pandoc-template.tex -o $@ $<

%.html: %.md $(BIB)
	$(PANDOC) --to html5 --css pandoc.css --default-image-extension=png --self-contained -o $@ $<

#####################
# END PANDOC RULES  #
#####################

clean: 
	$(RM) *.log *.aux *.toc *.tof *.tog *.bbl *.blg *.pdfsync *.d *.dvi *.out *.thm vc.tex *.nav *.snm

reallyclean: clean
	$(RM) *.pdf

# vim: set noexpandtab :
